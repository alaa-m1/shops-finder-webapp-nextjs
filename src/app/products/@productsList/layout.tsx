import React from "react";

const ParallelProductsLayout = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  return (
    <>
      {children}
    </>
  );
};
export default ParallelProductsLayout;
