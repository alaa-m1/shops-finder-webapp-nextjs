import multer from "multer";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "/temp");
  },
  filename: function (req, file, cb) {
    console.log('req.body.fullName===',req.body.fullName)
    cb(null, req.body.fullName);
  },
  
});

const multerUpload = multer({ storage: storage });

export default multerUpload;
